import React, { Component } from 'react'
import {
  View,
  Text,
  StyleSheet,
  ActivityIndicator
} from 'react-native'

import Video from 'react-native-video'
import Layout from '../components/video-layout'
import ControlsLayout from '../components/controls-layout'
import PlayPause from '../components/play-pause'


class Player extends Component {

  state = {
    loading: true,
    paused: false
  }

  onBuffer = ({ isBuffering }) => {
    this.setState({
      loading: isBuffering
    })
  }

  onLoad = () => {
    this.setState({
      loading: false
    })
  }


  playPause = () => {
    this.setState({
      paused: !this.state.paused 
    })
  }


  render() {
    return (
      <Layout
        loading={this.state.loading}
        video={
          <Video
            source={{ uri: 'https://download.blender.org/peach/bigbuckbunny_movies/BigBuckBunny_320x180.mp4' }}
            style={styles.video}
            resizeMode="contein"
            onBuffer={this.onBuffer}
            onLoad={this.onLoad}
            paused={this.state.paused}
          />}

        loader={<ActivityIndicator color={'red'} />}
        controls={
          <ControlsLayout>
            <PlayPause 
              onPress={this.playPause}
              paused={this.state.paused}
            />
            <Text>progress bar | </Text>
            <Text>time left | </Text>
            <Text>fullScreen | </Text>
          </ControlsLayout>
        }
      />
    )
  }
}

const styles = StyleSheet.create({
  video: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    top: 0
  }
})


export default Player


